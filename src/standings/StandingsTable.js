import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {PlayerGameScore} from './components';

class StandingsTable extends Component {
  render() {
    const {games, standings} = this.props;

    return (
      <table>
        <thead>
          <tr>
            <th>Place</th>
            <th>Player</th>
            {games.map(game => (
              <th key={game.number}>{game.number}</th>
            ))}
            <th>Total</th>
            <th>AVG</th>
          </tr>
        </thead>
        <tbody>
          {standings.map((playerRow, index) => {
            return (
              <tr key={playerRow.playerId}>
                <td>{index + 1}</td>
                <td>{playerRow.playerName}</td>
                {games.map(game => (
                  <PlayerGameScore playerScores={playerRow.scores} gameNumber={game.number} />
                ))}
                <td>{playerRow.totalScore}</td>
                <td>{playerRow.averageScore}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
}

StandingsTable.propTypes = {
  games: PropTypes.array.isRequired,
  standings: PropTypes.array.isRequired,
};

export default StandingsTable;
