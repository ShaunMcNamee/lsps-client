import React from 'react';
import PropTypes from 'prop-types';

const PlayerGameScore = ({playerScores, gameNumber}) => {
  const playerGame = playerScores.find(playerScore => playerScore.gameNumber === gameNumber);

  if (!playerGame) {
    return <td key={gameNumber}>N/A</td>;
  }

  return <td key={gameNumber}>{playerGame.gameScore}</td>;
};

PlayerGameScore.propTypes = {
  playerScores: PropTypes.array.isRequired,
  gameNumber: PropTypes.number.isRequired
};

export default PlayerGameScore;