import React, {Component} from 'react';
import StandingsTable from './standings';

class App extends Component {
  state = {
    standings: [],
    games: [],
  };

  async componentDidMount() {
    const standings = await fetch('http://18.218.65.133:8080/api/standings').then(res => res.json());
    const games = await fetch('http://18.218.65.133:8080/api/games/2018').then(res => res.json());

    this.setState(() => ({
      standings,
      games: games.sort((game1, game2) => game1.number - game2.number),
    }));
  }

  render() {
    const {standings, games} = this.state;

    return (
      <div>
        <StandingsTable standings={standings} games={games} />
      </div>
    );
  }
}

export default App;
